const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/delilah');

const tokenCheck = mongoose.model('tokens' , {
    id : Number,
    token: String,
    isAdmin: Number
});
module.exports = { mongoose, tokenCheck}