const { route } = require("../routes/adduser");

const server = () => {
    const express = require("express");

const bodyParse = require("body-parser");
const app = express();
const PORT = 8080;
app.use(express.json());
app.use(bodyParse.json());
const router = express.Router();
app.use('/api', router);
app.listen(PORT, ()=> {
    console.log('Servidor corriendo en puerto 8080');
    
});
const listProductsImport = require("../routes/listProducts");
const loginUser = require("../routes/login")
const addUserImport = require("../routes/adduser");
const userData = require('../routes/users')
const listOrders = require('../routes/orders')
const addOrder = require('../routes/addOrder');
const orderStatus = require('../routes/updateOrderStatus')
const addProduct = require('../routes/addproducts')
const editProduct = require('../routes/editproducts')
const deleteProduct = require('../routes/deleteproducts')
const deleteOrder = require('../routes/deleteorder')
const editOrder = require('../routes/editorder')
const editUser = require('../routes/editusers')
const deleteUser = require('../routes/deleteusers')




router.use('/', listProductsImport);
router.use('/', loginUser);
router.use('/', addUserImport);
router.use('/', userData)
router.use('/', listOrders)
router.use('/', addOrder);
router.use('/', orderStatus);
router.use('/', addProduct);
router.use('/', editProduct);
router.use('/', deleteProduct);
router.use('/', deleteOrder);
router.use('/', editOrder)
router.use('/', editUser)
router.use('/', deleteUser)
}
module.exports = {server};