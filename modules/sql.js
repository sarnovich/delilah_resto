const database = () => {
const Sequelize = require ("sequelize")
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('./sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });

  db.authenticate()
  .then(()=> console.log('Connection has been established successfully.'))
    
  .catch ((error) => console.error('Unable to connect to the database:', error));
}
module.exports = {database}