# Delilah_resto

Welcome to Delilah_Resto
It is a backend project for Acamica®


#Environment
    #Software requirement
        * NodeJS || Go to https://nodejs.org/es/download/ and download and install NodeJS in your O.S
        * NPM || check if NPM is installed on your terminal type npm --version if npm is not installed go to Node home page to download and install
        * MySQL8 https://dev.mysql.com/downloads/
        * MongoDB https://www.mongodb.com/try/download/community

    #Download files
        * clone a repository gitlab.com/sarnovich/delilah_resto, if you have git installed you can use git clone git@gitlab.com:sarnovich/delilah_resto.git in your target directory, else, you can   fetch https://gitlab.com/sarnovich/delilah_resto.git

    #Package requirement
        * express
        * router
        * body-parser
        * sequelize
        * mysql2
        * jsonwebtoken
        * mongoose
    Go to destination folder and run on your terminal npm i express router body-parser sequelize mysql2 jsonwebtoken mongoose

        #Optional packages
            *Postman
    

#Database
        * in MySQL create the user "ivan" with the password "xx" - CREATE USER 'ivan'@'%' IDENTIFIED BY '2001403269';
        * create a database called delilah - CREATE SCHEMA `delilah` ;
        * Give permissions on the database to user ivan  - GRANT ALL PRIVILEGES ON *.* TO 'ivan'@'%';
        * If you have more knowledge of MySQL, you can create a user and grant permissions for localhost and specific databases.
        * Import sql database using mysql -u ivan -p < 'sqlstructure.sql' command, MySql Workbench or similar. The file is in SQL folder, please, after import remove it.
        * Start or restart the service, make sure your TCP 3306 port is free and "open" (check firewalls and anti virus.) and your my.cnf file
        * If you choice for use others credentials, you can modify the file ~/modules/sql-connection.js

#First use:
        * run node main.js in your terminal, after that you need create two or tree users
        * Create an admin user ( "isadmin": 1) and a non privileged user  ("isadmin": 0)
        * you can use postman exameples to do it, you can find a json file to import the collection in postman folder, please remove it after import
        * With the "admin" user can create a products to test (login first)
        * You need a login a user to use any router exception to /adduser (ok, yes, that's true) /login /products
        * You need login as a user with admin previleges to use /orderstatus /addproduct /editproduct /deleteproduct and others end points.