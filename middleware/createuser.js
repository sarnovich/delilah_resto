//const { response } = require('express');
const express = require('express')
const router = express.Router();

router.post('/adduser', (req, res, next) => {
    
    let elem = req.body
    
    if (elem.first_name == undefined || elem.first_name == "") {
        res.status(409).json("El campo nombre es obligatorio")
    } else if (elem.last_name == undefined || elem.last_name == "") {
        res.status(409).json("El campo apellido es obligatorio")
    } else if (elem.email == undefined || elem.email == "") {
        res.status(409).json("El campo correo es obligatorio")
    } else if (elem.username == undefined || elem.username == "") {
        res.status(409).json("El campo usuario es obligatorio")
    } else if (elem.address == undefined || elem.address == "") {
        res.status(409).json("El campo direccion es obligatorio")
    } else if (elem.phonenumber == undefined || elem.phonenumber == "") {
        res.status(409).json("El campo telefono es obligatorio")
    } else if (elem.isadmin === undefined || elem.isadmin === "") {
        res.status(409).json("El campo admin es obligatorio")
    } else if (elem.password == undefined || elem.password == "") {
        res.status(409).json("El campo contraseña es obligatorio")
    } else{
        next()
    }

 });
 
 module.exports = router