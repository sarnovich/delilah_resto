const express = require('express')
const router = express.Router();
const mongo = require ('../modules/nosql')
const {privateKey, jwt} = require('../modules/jwt')


router.put('/*', (req, res, next) => {
    
    try {

        const token = req.headers.authorization.replace('Bearer ','')
        let verifyToken = jwt.verify(token, privateKey)
        
        if(verifyToken) { 
            
            let mongoToken;
            let mongoAdmin;
            const checkToken =  mongo.tokenCheck.find({token: token}).then(function(result){
                mongoToken = (result[0].token)
                mongoAdmin = (result[0].isAdmin)
                
            })
            .then(() => {
                if (token == mongoToken && mongoAdmin === 1) {
                    
                    next()
                } else {
                    return res.status(403).json("Permiso Denegado")
                }
            })
        } else {
           return res.status(403).json("error de autenticacion")
        }
    } catch(err) {
        res.status(403).json({err: "bad token"})
    }
} 
)

module.exports = router
