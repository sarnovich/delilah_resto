const express = require('express')
const router = express.Router();
const mongo = require ('../modules/nosql')
const {privateKey, jwt} = require('../modules/jwt')



router.get('/*', (req, res, next) => {
    
    try {
        idQuery = req.query.id
        
        const token = req.headers.authorization.replace('Bearer ','')
        let verifyToken = jwt.verify(token, privateKey)
       
        if(verifyToken) { 
            
            let mongoToken;
            let mongoAdmin;
            const checkToken =  mongo.tokenCheck.find({token: token}).then(function(result){
                mongoToken = (result[0].token)
                mongoId = result[0].id
                mongoAdmin = (result[0].isAdmin)
                
            })
            //Me aseguro que el token enviado es correcto y que corresponde al cliente que 
            //dice ser, en su defecto, que sea admin.
            .then(() => {
            //dejo este console log para ver resultados
                console.log(`token: ${token}
                 Mongotoken = ${mongoToken}
                 verifyToken.getId = ${verifyToken.getId}, 
                 mongoId = ${mongoId}, 
                 idQuery = ${idQuery}
                 mongo admin = ${mongoAdmin}`)
                 
                if (((token == mongoToken) && (verifyToken.getId == mongoId) && (verifyToken.getId == idQuery) ) || mongoAdmin === 1) {
                    
                    next()
                } else {
                   return res.status(403).json("You can't see others users")
                }
            })
        } else {
            return res.status(403).json("error de autenticacion")
        }
    } catch(err) {
        
        res.status(403).json({err: "Forbidden"})
    }
})

module.exports = router