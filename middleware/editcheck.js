const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
 
router.put('/editorder', async (req, res, next) => {
 
 
    const elem = req.body;
    const targetId = req.query.id
    
    await db.query("delete FROM `product_order` WHERE " + "(order_id = " + targetId + ")" )
    
    .then((response) => {
        if (response[0].affectedRows == 0) {
            res.json("id no valido")
        } else {
            return next()
        }
  })
    .catch((err) => {
        res.json("error")
    
})
})

module.exports = router
