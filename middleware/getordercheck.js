const express = require('express')
const router = express.Router();
const mongo = require ('../modules/nosql')
const {privateKey, jwt} = require('../modules/jwt')
const Sequelize = require ("sequelize");
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection');
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  

/*
Esta funcion revisa los datos del token mas la nosql para mostrar la orden del cliente en particular o puede ver todas si es admin.
Para esto, lo primero que revisa es el token valido, si es asi, revisa que sea admin, en caso positivo se va con next().
Si no es admin, revisa que la orden solicitada sea del usurio que mando el token
*/
router.get('/*', (req, res, next) => {
    try {

        idQuery = req.query.id
        let mongoToken;
        let mongoAdmin;
        const token = req.headers.authorization.replace('Bearer ','')
        let verifyToken = jwt.verify(token, privateKey)
        let getIdUserOrder;
        let idUserOrder;

        if(verifyToken) { 
            const checkToken =  mongo.tokenCheck.find({token: token}).then(function(result){
                mongoToken = (result[0].token)
                mongoId = result[0].id
                mongoAdmin = (result[0].isAdmin)
                
                    })
                    .then(() => {
                        if(mongoAdmin == 1) {
                            console.log(mongoAdmin)
                            return next()
                        } else {
                            getIdUserOrder = db.query(`SELECT * FROM delilah.order where id=${idQuery};`,{ type: Sequelize.QueryTypes.SELECT })
                                .then((getIdUserOrder) => {
                                    idUserOrder = getIdUserOrder[0].users_id

                                    console.log(`token: ${token}
                                    Mongotoken = ${mongoToken}
                                    verifyToken.getId = ${verifyToken.getId}, 
                                    idUserOrder = ${idUserOrder}, 
                                    mongoId = ${mongoId}, 
                                    idQuery = ${idQuery}
                                    mongo admin = ${mongoAdmin}`)
                                    if (((token == mongoToken) && (verifyToken.getId == mongoId) && (verifyToken.getId == idUserOrder) ) ) {
                        
                                       return next()
                
                                        } else {
                                            return res.status(403).json("Forbidden")
                                        }
                                } ) .catch ((err) => {
                                    console.log("error", err)
                                    return res.status(403).json("Denegado")
                                })
                                
                        }
                    })

                    

        } else {
            return res.status(403).json("Forbidden")
        }



     } catch(error) {
        return res.status(403).json("Forbidden")
     } 


  
                                    })
module.exports = router