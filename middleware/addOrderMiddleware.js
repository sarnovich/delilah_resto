const express = require('express');
const router = express.Router();
const Sequelize = require ("sequelize");
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
const {privateKey, jwt} = require('../modules/jwt');
const bodyparser = require('body-parser');
  
  let idCheck;
  let paymentID = true
  let productCheck ;
  let tokenvalid = false
router.post('/addorder', async (req, res, next) => {
    let elem = req.body

    
   
   
   //check token
   
    
   try {
    const token = req.headers.authorization.replace('Bearer ','')
    let verifyToken = jwt.verify(token, privateKey);
    if (verifyToken) {
        tokenvalid = true
        
        }
    }  catch (err) {
       //verifyToken = false
        return res.status(403).json("token no valido")
    }
    //check product id
   
    let arrayProductsId = []
    for(let x = 0; x <= elem.products.length - 1; x++) {
        arrayProductsId.push(elem.products[x].products_id)
     }

    let checkId;
    for (let i = 0; i <= arrayProductsId.length -1; i++) {
        try {
        checkId = await db.query (`SELECT * FROM products where id = ${arrayProductsId[i]};`,{ type: Sequelize.QueryTypes.SELECT })
        
            if(checkId) {
                productCheck = true
            }
        }
        catch (err) {
            
            productCheck = false
        }

    // check user id

    idCheck = await db.query(`SELECT * FROM users where id = ${elem.users_id};`,{ type: Sequelize.QueryTypes.SELECT })
        .then((response) => {
            
            if(response[0].id == elem.users_id) {
               
                return true
            }  else {
                return false
                };
        })

     .catch((err) =>  {
        idCheck = false
        res.status(400).json("Id de usuario no valido")
        });     
        

    // Check pay method

    idCheck = await db.query(`SELECT * FROM payment_methods where id = ${elem.payment_methods_id};`,{ type: Sequelize.QueryTypes.SELECT })
    .then((response) => {
        
        if(response[0].id == elem.payment_methods_id) {
           
            return true
        }  else {
            return false
        };
    })

 .catch((err) =>  {
    idCheck = false
    res.status(400).json("Id de pago no valido")
    }); 
      }
    

    if (tokenvalid == false || elem.address == null || idCheck != true || paymentID != true || productCheck != true) {
        console.log( tokenvalid,  elem.address, idCheck, paymentID, productCheck )
        res.status(400).json("revisar datos")
    } else {
        next();
    }
    
})
 module.exports = router