//Listar productos
const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  

    
router.get('/products', async (req, res, next) => {
    await db.query('SELECT * FROM products;',{ type: Sequelize.QueryTypes.SELECT })
    .then((response) => {
        
        res.json(response)
        next();
    })
    .catch((err) => console.log(err) )  
});
module.exports = router