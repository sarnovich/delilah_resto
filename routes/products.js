const productos = () => {
const { server } = require("../modules/express")

const express = require("express");
const routes = require('express').Router();
const db = require('../modules/sql')

routes.get('/products', async (res,req) => {
    const result = await db.query('select * from products')
    res.status(200).send(result);
})
}
module.exports = { productos}