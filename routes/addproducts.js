const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const isAdmin = require ('../middleware/admincheckpost')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  

    
router.post('/addproducts', isAdmin, async (req, res, next) => {
   
    
    let elem = req.body
    
    await db.query('INSERT INTO `products` (`name`, `price_unit`, `description`, `url_img`) VALUES (?, ?, ?, ?)', {
        replacements : [elem.name, elem.price_unit, elem.description, elem.url_img]  } )
   
    .then((response) => {
        
        res.status(201).json("producto creado con exito.")
        
        next();
    })
    .catch((err) => {
      
       res.status(409).json("Revisar los datos.")
        
        next(); 
    
    
});
})

module.exports = router