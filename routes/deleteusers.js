const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const isAdmin = require ('../middleware/admincheckdelete')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
router.delete('/deleteuser', isAdmin, async (req, res, next) => {
    

    const targetId = req.query.id;
    
    await db.query("delete FROM `users` WHERE " + "(id = " + targetId + ")" )
    .then((response) => {
        
        res.status(201).json("Usuario Eliminado")
        
        next();
    })
    .catch((err) => {
       console.log(err)
       res.status(409).json("Revisar los datos.")
        
        next(); 
});
  })

  module.exports = router
    