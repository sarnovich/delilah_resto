const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const addOrderMiddleware = require ('../middleware/addOrderMiddleware')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
  let consult;  
  let totalPrice = 0;
  const actualDate = new Date();


router.post('/addorder', addOrderMiddleware, async (req, res, next) => {
  console.log(actualDate)
        let elem = req.body;
      try {
        for (let i = 0; i <= elem.products.length -1; i ++) {
            
            consult = await db.query(`SELECT price_unit from products WHERE id = ${elem.products[i].products_id}`, { type: Sequelize.QueryTypes.SELECT })
            
            .then ((consult) => {
            
                totalPrice = totalPrice + (consult[0].price_unit * elem.products[i].quantity)
              }) 
            } } catch (error) {
              res.status(500).json("interal server error")
            }  
        let dbQuery = []
        console.log(actualDate)
        dbQuery = await db.query('INSERT INTO `order` (`total_price`, `date`, `description`, `address`, `status_id`, `payment_methods_id`, `users_id`) VALUES (?, ?, ?, ?, ?, ?, ?)', {
            replacements : [totalPrice, actualDate, elem.description, elem.address, elem.status_id, elem.payment_methods_id, elem.users_id]  } )
            

            .then((dbQuery) => {

              for (let x = 0; x <= elem.products.length -1; x ++) {
            

                db.query('INSERT INTO `product_order` (`quantity`, `order_id`, `products_id`) VALUES (?, ?, ?)', {
                replacements : [elem.products[x].quantity, dbQuery[0], elem.products[x].products_id] })
          
            } 
        
                res.status(201).json(`La orden numero ${dbQuery[0]} fue cargada con exito.`)
                next();
        })
            .catch((err) => {
              console.log(err)
            
            res.status(400).json("Error en los parametros, no se cargo la orden") })  
   
    });
module.exports = router

