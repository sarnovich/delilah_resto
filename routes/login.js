/*El login aparte de revisar en la base de datos relacional, 
tambien se guarda el id, genera el token que es enviado al front
y guarda un registro en mongodb con token - id  y si es admin para luego realizar un doble checkeo en los middleware
sin usar la base relacional. */
const express = require('express')
const router = express.Router();
const {jwt, privateKey} = require ('../modules/jwt')
const Sequelize = require ("sequelize");
const authUserMiddleware = require ('../middleware/authuser')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
  const mongo = require ('../modules/nosql')
 
  let dbQuery = []
  router.post('/login', authUserMiddleware, async (req, res, next) => {
    const {user, password} = req.body
    
    dbQuery =  await db.query(`SELECT * from users where username = "${user}" and password = "${password}" limit 1;`,
     { type: Sequelize.QueryTypes.SELECT })
   
    .then((dbQuery) => {
    
     let getId;
         for (const prop in dbQuery) {
            getId = dbQuery[prop].id   
        }

    let getIsAdmin;
        for (const prop in dbQuery) {
            getIsAdmin = dbQuery[prop].isadmin 
        }
        if (getId > 0) {
            console.log("Id de usuario para addorder ",getId)
            const token = jwt.sign({user, getId}, privateKey)
            let createMongoDoc = {
                id : getId,
                token: token,
                isAdmin: getIsAdmin
            }
            
            const saveToMongo = new mongo.tokenCheck(createMongoDoc)
            saveToMongo.save()
            res.status(201).json(token)
            mongo.tokenCheck.find({isAdmin: 1}).then(function(result){
                
            });
           
        } else {
            res.status(401).json("Wrong username or password")
        }
        
        next();
    })
    .catch((err) => {
        console.log(err)
        res.status(500).json("Internal server error")
         })  
});


module.exports = router

