const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const isAdmin = require ('../middleware/admincheckput')
const editCheck = require ('../middleware/editcheck')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
  let consult;  
  let totalPrice = 0;
  let deleted;
  const newDate = new Date()
  let actualDate = newDate.toISOString().slice(0, 19).replace('T', ' ')
router.put('/editorder', isAdmin, editCheck, async (req, res, next) => {
 
 
    const elem = req.body;
    const targetId = req.query.id

  totalPrice = 0

    for (let i = 0; i <= elem.products.length -1; i ++) {
            
      consult = await db.query(`SELECT price_unit from products WHERE id = ${elem.products[i].products_id}`, { type: Sequelize.QueryTypes.SELECT })
      
      .then ((consult) => {
      
          totalPrice = totalPrice + (consult[0].price_unit * elem.products[i].quantity)
          
        }) 
      }  
      
  let dbQuery = []
  
  dbQuery = await db.query("UPDATE `order` SET `total_price` = "+ `"${totalPrice}"` + 
  ", `date` = " + `"${actualDate}"` + ", `description` = " + `"${elem.description}"` + 
  ", `address` = " + `"${elem.address}"` + ", `status_id` = " + `"${elem.status_id}"` + 
  ", `payment_methods_id` = " + `"${elem.payment_methods_id}"`+ ", `users_id` = " + 
  `"${elem.users_id}"` + " WHERE " + "(id = " + targetId + ")" )
  
  
      
  
      .then((dbQuery) => {

        for (let x = 0; x <= elem.products.length -1; x ++) {
           console.log(elem.products[x].quantity)
           console.log(targetId)
           console.log(elem.products[x].products_id)

      try {
           db.query('INSERT INTO `product_order` (`quantity`, `order_id`, `products_id`) VALUES (?, ?, ?)', {
            replacements : [elem.products[x].quantity, targetId, elem.products[x].products_id] })
           
          } catch {
            console.log("error")
          }
          
    
      } 
  
          res.status(201).json(`La orden numero ${targetId} fue modificada con exito.`)
          next();
  })
      .catch((err) => {
       console.log(err)
      
      res.status(400).json("Error en los parametros, no se cargo la orden") })  

});



  module.exports = router
    