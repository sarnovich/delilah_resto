//Listar productos

const express = require('express')
const router = express.Router();
const Sequelize = require ("sequelize");
const creteUserMiddleware = require ('../middleware/createuser')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });    
router.post('/adduser', creteUserMiddleware, async (req, res, next) => {
    
    let elem = req.body
    
    await db.query('INSERT INTO `users` (`first_name`, `last_name`, `email`, `username`, `address`, `phonenumber`, `isadmin`, `password`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', {
        replacements : [elem.first_name, elem.last_name, elem.email, elem.username, elem.address, elem.phonenumber, elem.isadmin, elem.password]  } )
   
    .then((response) => {
        
        res.status(201).json("User created")
       
        next();
    })
    .catch((err) => {
       console.log(err.errors)
       res.status(409).json(`Some parameters are not correct. ${err.errors}`)
        
        next(); 
    
    
});
})

module.exports = router