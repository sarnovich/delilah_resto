const express = require('express')
const router = express.Router();
const {jwt, privateKey} = require ('../modules/jwt')
const Sequelize = require ("sequelize");
const isAdmin = require ('../middleware/admincheckput')
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  
 
  router.put('/orderstatus', isAdmin, async (req, res, next) => {
    const elem = req.body;
    
    await db.query("UPDATE `order` SET `status_id` = "+ elem.status_id + " WHERE " + "(id = " + elem.id + ")" )
    .then((response) => {
        
        res.status(201).json("Pedido actualizado")
        next();
    })
    .catch((err) => {
       console.log(err.errors)
       res.status(409).json("Revisar los datos.")
        
        next(); 
});

  })

  module.exports = router
