const express = require('express')
const router = express.Router();
const middlewareCheckUsers = require ('../middleware/getusercheck')
const Sequelize = require ("sequelize");
const { dbName, dbUser, dbPass, dbHost, dbDialect} = require('../modules/sql-connection')
const db = new Sequelize( dbName, dbUser, dbPass, {
    host: dbHost,
    dialect: dbDialect
  });  

// no params response all users (only admin)
// one params, response data from user.
router.get('/users/', middlewareCheckUsers, async (req, res, next) => {
    try {
    const id = req.query.id
    if ( id === undefined) {
        await db.query('SELECT * FROM users;',{ type: Sequelize.QueryTypes.SELECT })
    .then((response) => {
        
        res.status(200).json(response)
        
    })
    .catch((err) => console.log(err) )
    } else {
        await db.query(`SELECT * FROM users where id=${id};`,{ type: Sequelize.QueryTypes.SELECT })
    .then((response) => {
        
        res.status(200).json(response)
        
    })
    .catch((err) => console.log(err) )
    }
    
res.status(409).json("Revisar los datos.")
}
catch(error){
}
});
module.exports = router